class AddIndexToUsername < ActiveRecord::Migration[6.1]
  def change
    add_index :users, :user_name
  end
end
