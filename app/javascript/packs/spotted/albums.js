
import 'stylesheets/spotted/albums'

import { Modal } from 'bootstrap'

document.querySelector('.btn-create-new-album')?.addEventListener('click', ()=>{
  let modal_new_album_element = document.querySelector('#modal-album')
  if(modal_new_album_element){
    let modal_new_album = new Modal(modal_new_album_element);
    modal_new_album.show();
  }
})

// Watchin changes in the modal edit container 
let modal_edit_container = document.querySelector('.modal-edit-container')
if(modal_edit_container){
  let modal_edit_container_observer = new MutationObserver(()=>{
    let modal_edit_element = modal_edit_container.querySelector('#modal-album')
    let modal_edit_object = new Modal(modal_edit_element)
    modal_edit_object.show()
  });
  modal_edit_container_observer.observe(modal_edit_container, { childList: true })
}

