module ApplicationHelper

  #  Verifies if the user is logged in
  #
  # @return [Boolean] the logged in verification for user
  #
  def logged_in?
    if session[:user_id].present? and !session[:user_id].nil?
      true
    else
      false
    end
  end

  #
  # Returns the current user in the application
  #
  # @return [User] model User with the user data
  #
  def current_user
    User.find(session[:user_id]) if session[:user_id]
  end

  def authorized?
    redirect_to spotted_artists_index_path if logged_in?
  end

  def not_authorized?
    redirect_to root_path unless logged_in?
  end

end
