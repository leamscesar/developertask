module SessionsHelper

  # Creates a session variable for user id
  #
  # @param [User] user The model object with user data
  #
  def user_login(user)
    session[:user_id] = user.id unless session[:user_id].present?
  end

  #
  # Removes the session variable containing the current user
  #
  def user_logout
    session.delete(:user_id)
  end

end
