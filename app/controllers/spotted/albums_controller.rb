class Spotted::AlbumsController < Spotted::SpottedController
  def index
    @artist = Artist.find(params[:artist_id])
    @albums = @artist.albums
    @new_album = Album.new
  end

  def edit
    @album = Album.find(params[:id])
    respond_to do |format|
      format.js do
        render 'spotted/albums/edit',
               locals: {
                 album: @album,
                 artist: @album.artist,
                 albums_action_path: spotted_album_path(@album),
                 method: :put
               }
      end
    end
  end

  def update
    @album = Album.find(params[:id])
    @artist = @album.artist
    ActiveRecord::Base.transaction do
      respond_to do |format|
        if @album.update(album_params)
          @albums = @artist.albums
          format.js { render 'spotted/albums/index', locals: { albums: @albums } }
        else
          format.js { render inline: "alertify.error(\"#{@album.errors.full_messages.join('\n')}\")" }
        end
      end
    end
  end

  def new
  end

  def create
    @new_album = Album.new(album_params)
    ActiveRecord::Base.transaction do
      respond_to do |format|
        if @new_album.save
          @albums = @new_album.artist.albums
          format.js { render 'spotted/albums/index', locals: { albums: @albums } }
        else
          format.js { render inline: "alertify.erro(\"#{@new_album.errors.full_messages.join('\n')}\")"}
        end
      end
    end
  end

  def destroy
    @album = Album.find(params[:id])
    @artist = @album.artist
    ActiveRecord::Base.transaction do
      respond_to do |format|
        if @album.delete
          @albums = @artist.albums
          format.js { render 'spotted/albums/index', locals: { albums: @albums } }
        else
          format.js { render inline: "alertify.erro(\"#{@album.errors.full_messages.join('\n')}\")"}
        end
      end
    end
  end

  private

  def album_params
    params.require(:album).permit(Album.column_names - %i[id created_at updated_at])
  end


end
