class Spotted::ArtistsController < Spotted::SpottedController
  def index
    @artists = Artist.all
  end
end
