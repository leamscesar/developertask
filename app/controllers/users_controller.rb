class UsersController < ApplicationController

  include SessionsHelper

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      user_login(@user)
      redirect_to spotted_artists_index_path
    else
      @error_message = @user.errors.full_messages.join('\n')
      render :create
    end
  end

  def show
  end

  def edit
  end

  def update
  end

  def destroy
  end

  private

  def user_params
    params.require(:user).permit(User.column_names - %w[id created_at updated_at],
                                 %i[password password_confirmation])
  end

end
