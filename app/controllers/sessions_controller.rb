class SessionsController < ApplicationController
  include SessionsHelper

  before_action :authorized?, only: %i[new create]

  def new
    @user = User.new
  end

  def create
    @user = User.find_by(user_name: user_params[:user_name])
    if !@user.nil? && @user.authenticate(user_params[:password])
      user_login @user
      redirect_to spotted_artists_index_path
    else
      @message = "Sorry, we couldn't find an account with this username. Please check you're using the right username and try again."
      respond_to do |format|
        format.js { render js: "alertify.alert(\"Login error\", \"#{@message}\")", layout: false }
      end
    end
  end

  def destroy
    user_logout
    redirect_to root_path
  end

  private

  def user_params
    params.require(:user).permit(User.column_names - %i[id created_at updated_at password_digest],
                                 :password)
  end

end
