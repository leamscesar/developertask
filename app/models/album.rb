class Album < ApplicationRecord
  include ArtistsConcern

  validates :album_name, presence: true
  validates :year, presence: true

  def artist
    Artist.new(find_artist_by_id(artist_id))
  end
end
