# File dedicated to implementing the base class of artist
class Artist
  extend ArtistsConcern

  attr_accessor :id, :twitter, :name

  def initialize(artist_hash)
    @id = artist_hash['id']
    @name = artist_hash['name']
    @twitter = artist_hash['twitter']
  end

  def self.all
    all_artists.map do |artist_hash|
      new(artist_hash)
    end
  end

  def self.find(id)
    new(find_artist_by_id(id))
  end

  def albums
    Album.where(artist_id: id)
  end

end
