# Concern dedicated to making request to the api
module ArtistsConcern
  extend ActiveSupport::Concern
  include HTTParty

  BASE_URI = 'https://moat.ai/api/task/'
  BASIC_HEADER = 'ZGV2ZWxvcGVyOlpHVjJaV3h2Y0dWeQ=='

  def all_artists
    http_response = HTTParty.get(BASE_URI, headers: { 'Basic': BASIC_HEADER })
    parsed_http_response = http_response&.parsed_response
    parsed_http_response&.map do |response_item|
      response_item[0]
    end
  end

  def find_artist_by_id(artist_id)
    http_response = HTTParty.get(BASE_URI + "?artist_id=#{artist_id}", headers: { 'Basic': BASIC_HEADER })
    http_response&.parsed_response.try(:[], 0)
  end

end
