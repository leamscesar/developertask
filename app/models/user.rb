class User < ApplicationRecord
  has_secure_password

  validates :user_name, format: { with: /[A-Za-z_0-9]+/i }, presence: true, uniqueness: true
  validates :full_name, presence: true
  validates :password, presence: true
  validates :role, presence: true

  enum role: { user: 0, admin: 1 }

end
