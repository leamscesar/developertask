require "test_helper"

class Spotted::ArtistsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get spotted_artists_index_url
    assert_response :success
  end
end
