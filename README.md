# Developer Task

* A simple albums creation app consuming an api provided by the moat bulders

This app relies on figaro gem for storing environment variables, thus you'll have
to create a file config/application.yml following the template

```yml
  <environment_name>: 
    db_user: 'username'
    db_psw: 'user_password'
    db_host: 'user_host'
    db_port: 'user_host_port'
    db_name: 'user_local_db'
```

And the database.yml file can be configured as follows: 
```yml
default: &default
  adapter: postgresql
  encoding: unicode
  host: <%= Figaro.env.db_host %>
  port: <%= Figaro.env.db_port %>
  username: <%= Figaro.env.db_user %>
  password: <%= Figaro.env.db_psw %>
  pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>

<environment_name>:
  <<: *default
  database: <%= Figaro.env.db_name %>
```

This project also uses rails version 6 with webpacker.
