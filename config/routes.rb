Rails.application.routes.draw do

  root to: 'sessions#new'
  post 'sessions/create'
  delete 'sessions/destroy'

  #get 'users/new'
  #post 'users/create'
  #get 'users/show'
  #get 'users/edit'
  #put 'users/update'
  #delete 'users/destroy'
  resources :users

  namespace :spotted do
    get 'artists/index'

    # albunst actions
    get 'albums/index/:artist_id', to: 'albums#index', as: :artist_albums_index
    resources :albums
    #get ':artist_id/albuns/edit/:id', to: 'albuns#edit', as: :albuns_edit
    #put ':artis_id/albuns/update/:id', to: 'albuns#update', as: :albuns_update
    #get 'albuns/new', 
    #post 'albuns/create'
    #delete 'albuns/destroy'
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
